const express = require('express');
const bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use(express.static('public'));

const routerApi = require("./routes/api")
app.get('/', function (req, res) {
  res.sendFile(`${__dirname}/views/dashboard.html`)
})
app.use('/api',routerApi)
app.listen(3000)
