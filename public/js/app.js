new Vue({
	el:'#app',
	data:()=>{
		return {
			nombre:null,
			msg:'Hola Mundo'
		}
	},
	methods: {
		saludar:function(){
			axios.post('/api/saludar',{
				nombre:this.nombre
			}).then(res=>{
				this.msg=res.data.msg
			});
			// console.log("Para saludar")
		}
	}
})