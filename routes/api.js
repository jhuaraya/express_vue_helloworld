let express = require('express');
let router = express.Router();

router.get('/',(req,res)=>{
	return res.json({msg:'Inicio de api'})
})

router.post('/saludar',(req,res)=>{
	
	return res.json({msg:`Hola ${req.body.nombre}`})
});

router.get('**',(req,res)=>{
	return res.json({success:false,msg:'No existe Eruta'})
})
module.exports = router;